FROM node:12.2.0

ENV TERRAFORM_VERSION=0.12.25

# ssh-keyscan to access Bitbucket repos
ARG BITBUCKET_SSH_KEY
RUN mkdir /root/.ssh/
COPY .codeship_pro/codeship_id_rsa.pub /root/.ssh/id_rsa.pub
RUN echo $BITBUCKET_SSH_KEY >> /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa
RUN ssh-keyscan -H bitbucket.org >> /root/.ssh/known_hosts

RUN apt-get update && \
    apt-get install -y curl jq python bash ca-certificates git openssl unzip wget && \
    cd /tmp && \
    wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/bin

RUN apt-get update && apt-get install -y \
    dnsutils
    
RUN mkdir -p /terraform

WORKDIR /terraform/infrastructure/


ENV TF_LOG DEBUG
ENV TF_LOG_PATH /tf_log.txt
ARG PRIVATE_SSH_KEY_AWS_INSTANCE_REPO 
RUN bash -c "echo -e $PRIVATE_SSH_KEY_AWS_INSTANCE_REPO > /root/ec2-launch-key.pem"
RUN chmod 600 /root/ec2-launch-key.pem
COPY . /terraform
