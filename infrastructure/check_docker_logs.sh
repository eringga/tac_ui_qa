for CONTAINER_ID in `docker container ls | grep -v CONTAINER | awk '{print $1}'`
  do
     echo '-----------------------------------'
     echo 'container_id='$CONTAINER_ID
     echo 'name='`docker inspect $CONTAINER_ID | grep '"Name":'`
     echo '-----------------------------------'
     
     echo '--logs-start---'
     docker logs $CONTAINER_ID --tail 20
     echo '--logs-done---'
     echo '-----------------------------------'
done
