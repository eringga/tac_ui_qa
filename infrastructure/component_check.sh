export BRANCH_NAME=$1
export KEY_PATH=$2
export USERNAME=$3

for COMPONENT in tac-node apvm dovm 
  do
     echo 'component='$COMPONENT
     export INSTANCE_IDS=`aws ec2 describe-instances --filters Name="tag:Name",Values="$COMPONENT*$BRANCH_NAME*" Name="instance-state-code",Values=16 --region ca-central-1 --query "Reservations[*].Instances[*].InstanceId" | jq '.[][]' | sort | uniq | sed "s/\"//g"`
     echo 'instance_ids='$INSTANCE_IDS
     for INSTANCE_ID in $INSTANCE_IDS
      do
     	INSTANCE_DATA=`aws ec2 describe-instances --filters "Name="instance-id",Values=$INSTANCE_ID" --region ca-central-1 | jq ".Reservations[].Instances[]"`
     	export IP=`echo $INSTANCE_DATA | jq .PublicIpAddress | sed "s/\"//g"`
     	export TAG_VALUES=`echo $INSTANCE_DATA | jq .Tags[].Value | sed "s/\"//g"`
     	export TAG_KEYS=`echo $INSTANCE_DATA | jq .Tags[].Key | sed "s/\"//g"`
     	echo '---------------------------------------------'
     	echo 'component='$COMPONENT
     	echo 'ip='$IP
     	echo 'tag_keys='$TAG_KEYS
     	echo 'tag_values='$TAG_VALUES
     	echo '---------------------------------------------'
     	cmd="sudo docker container ls" 
     	echo '---------------------------------------------'
     	echo 'cmd='$cmd
     	echo '---------------------------------------------'
     	ssh -o StrictHostKeyChecking=no -i $KEY_PATH $USERNAME@$IP $cmd
  done
done
