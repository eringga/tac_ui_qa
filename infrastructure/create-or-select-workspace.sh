#!/bin/bash

ws=$1
commit_id=$2
#convert slashes to hyphens for workspace creation
stripped=$(echo $1 | tr / -)
terraform workspace select ${stripped} || terraform workspace new ${stripped} && terraform workspace select ${stripped}
return $?