

#!/bin/bash -eux

export TEST_ENV=$(terraform output project_id)

# perform a health check
bash proxy_check.sh $TEST_ENV /root/ec2-launch-key.pem ubuntu

bash component_check.sh $TEST_ENV /root/ec2-launch-key.pem ubuntu
