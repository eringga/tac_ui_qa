
module "tac-network" {
  source     = "git@bitbucket.org:viascience_dev/via-tf-modules.git//tac-network?ref=v3.7.0"
  project_id = "qa-${terraform.workspace}"
  conda_api_key=var.conda_api_key
  network_config = {
    ffpy_environment_file_address    = "https://s3-us-west-2.amazonaws.com/tac-free-form-python-dev/environments/environment_files/"
    ffpy_environment_list            = "https://s3-us-west-2.amazonaws.com/tac-free-form-python-dev/environments/environment_list.csv"
    ffpy_output_example_file_address = "https://s3-us-west-2.amazonaws.com/tac-free-form-python-dev/outputs/example_files/"
    ffpy_outputs_list                = "https://s3-us-west-2.amazonaws.com/tac-free-form-python-dev/outputs/output_list.csv"
    um_address                       = "https://um.solvewithvia.com"
    um_port                          = "443"
    vs_hashes                        = "https://public-verified-scripts-dev.s3.ca-central-1.amazonaws.com/hashes.csv"
    vs_jsons                         = "https://bitbucket.org/viascience_dev/public_verified_scripts/raw/"
  }
  self_signed_certs = "true"
  tac_version  = var.tac_version
  apvm_version = var.apvm_version
  dovm_version = var.dovm_version
  parent_hosted_zone_name = "dev.poweredbyvia.com"

  env          = "public-dev"
  tac_instance_size  = "t2.large"
  apvm_instance_size = "t2.large"
  dovm_instance_size = "t2.large"
  key_name           = "default-dev"
  accepted_cidrs     = ["0.0.0.0/0"] #Change to Montreal Office IP
}

