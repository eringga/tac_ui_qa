#!/bin/bash -eux

#Call the lambda function to promote AMIs
# arn:aws:lambda:ca-central-1:893823511921:function:map-versions-prod-promote-images
curl -X POST \
  'https://fcdje2c9hf.execute-api.ca-central-1.amazonaws.com/prod/promote-ami' \
  -H 'content-type: application/json' \