# Configure the AWS Provider
provider "aws" {
  version = "~> 2.34"
  region  = "ca-central-1"
}

terraform {
  backend "s3" {
    bucket         = "terraform-state-via-dev"
    key            = "tac-qa"
    region         = "ca-central-1"
    dynamodb_table = "terraform-state-lock-dynamo-via-dev"
    workspace_key_prefix = "tac-qa"
  }
}

