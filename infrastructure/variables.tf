
variable "conda_api_key" {
    description = "Secret for conda"
    type=string
}

variable "tac_version" {
    default = "v0.4.10-0959b41f2b0248106b2894b6a5f6b2aa4a0bd17a"
}

variable "apvm_version" {
    default = "v2.2.16-c375872649d98362bb6cdd2d9a96b906017eaca8"
}

variable "dovm_version" {
    default = "v1.1.5-3482e602869d519afa986fae84860fc0a4f5c457"
}
