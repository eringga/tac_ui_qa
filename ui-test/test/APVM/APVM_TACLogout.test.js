import webdriver from 'selenium-webdriver';

import { helper, getElementById, start, stop, getElementByName, getElementByXpath, getCapabilities, Login, Logout, ConfigWebdriver, SauceLabsConfig, getText } from '../../utils';

const capabilities = getCapabilities('APVM TAC Logout', 'APVM');

describe('webdriver', () => {
    let driver = SauceLabsConfig(capabilities);
    ConfigWebdriver(driver, process.env.SITE_URL_APVM_NO_MARKET);

    test(
        'apvm-login',
        async () => {
            //get login
            await Login(driver, process.env.USERNAME_APVM, process.env.PASSWORD_APVM);
            //get element text Analytics Contracts
            await getText(driver, '//span[@class="toolbar-title"]', 'Analytics Contracts');
        },30000
    );

    test(
        'apvm-logout',
        async () => {
            //get logout
            await Logout(driver);
        },30000
    );

});
