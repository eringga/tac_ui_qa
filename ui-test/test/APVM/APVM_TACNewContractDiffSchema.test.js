import webdriver from "selenium-webdriver";

import {
  helper, getElementById, start, stop, getElementByName, getElementByXpath, getCapabilities, Login, ConfigWebdriver, SauceLabsConfig,
  SauceLabsConfigDual, ConfigWebdriverDual, setFormInput, clickButtonByPath, clickButtonById, setDate, selectOptionByIdPath,
  selectOptionById, getText, expectToEqualPath, setFormInputByPath, selectOptionByPath
} from '../../utils';

const capabilities = getCapabilities(
  "TAC New Contract 2 Datasets Diff Scehma",
  "APVM CreateNewContractDiffSchema"
);

describe("webdriver", () => {

  let driver = SauceLabsConfig(capabilities);
  let driver2 = SauceLabsConfig(capabilities);
  ConfigWebdriverDual(driver, driver2, process.env.SITE_URL_APVM_NO_MARKET, process.env.SITE_URL_DOVM);

  test(
    'dovm-create new database and datasets',
    async () => {
      await Login(driver2, process.env.USERNAME_DOVM, process.env.PASSWORD_DOVM);
      //get element data request
      const output = await getElementByXpath(driver2, '//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-data-request/mat-toolbar/span');
      const outputVal = await output.getText();
      expect(outputVal).toEqual("Data Requests");
      //click element menu data
      await clickButtonById(driver2, 'mat-expansion-panel-header-1');
      //click element menu database
      clickButtonByPath(driver2, '//div[@id="cdk-accordion-child-1"]/mat-action-row[1]/button');
      //click element button configure new database
      await clickButtonByPath(driver2, '//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-database/mat-toolbar/button');
      //element set form input database name
      await setFormInputByPath(driver2, '//input[@placeholder="Database Name"]', 'mydb21');
      //element set form input database description
      await setFormInputByPath(driver2, '//input[@placeholder="Database Description"]', 'database test 1')
      //element set form input database type
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database Type")]', '//mat-option/span[contains(.,"Postgres")]')
      //element set form input database version
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database Version")]', '//mat-option/span[contains(.,"9.5")]')
      //element set form input address
      await setFormInputByPath(driver2, '//input[@placeholder="Address"]', '209.97.173.183');
      //element set form input port
      await setFormInputByPath(driver2, '//input[@placeholder="Port"]', '5432');
      //element set form input database user
      await setFormInputByPath(driver2, '//input[@placeholder="Database User"]', 'eringga');
      //element set pawword form input
      await setFormInputByPath(driver2, '//input[@placeholder="Password"]', 'makeithappen');
      //click element button add
      clickButtonByPath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
      //click element button yes
      await clickButtonByPath(driver2, '//*[@id="mat-dialog-0"]/configure-database-dialog/div/button[2]');
      //wait until database created
      await getElementByXpath(driver2, '//*[@id="mat-dialog-0"]/configure-database-dialog/div/span')
      //click close button
      await clickButtonByPath(driver2, '//*[@id="mat-dialog-0"]/configure-database-dialog/mat-toolbar/button');

      //create database 2
      //click element button configure new database
      await clickButtonByPath(driver2, '//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-database/mat-toolbar/button');
      //element set form input database name
      await setFormInputByPath(driver2, '//input[@placeholder="Database Name"]', 'mydb22');
      //element set form input database description
      await setFormInputByPath(driver2, '//input[@placeholder="Database Description"]', 'database test 2')
      //element set form input database type
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database Type")]', '//mat-option/span[contains(.,"Postgres")]')
      //element set form input database version
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database Version")]', '//mat-option/span[contains(.,"9.5")]')
      //element set form input address
      await setFormInputByPath(driver2, '//input[@placeholder="Address"]', '209.97.173.183');
      //element set form input port
      await setFormInputByPath(driver2, '//input[@placeholder="Port"]', '5432');
      //element set form input database user
      await setFormInputByPath(driver2, '//input[@placeholder="Database User"]', 'eringga');
      //element set pawword form input
      await setFormInputByPath(driver2, '//input[@placeholder="Password"]', 'makeithappen');
      //click element button add
      await clickButtonByPath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
      //click element button yes
      await clickButtonByPath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
      // wait until database created
      await getElementByXpath(driver2, '//html/body/div[2]/div[2]/div/mat-dialog-container/configure-database-dialog/div/button/span/span[contains(.,"ADDED")]')
      //click close button
      await clickButtonByPath(driver2, '//*[@id="mat-dialog-1"]/configure-database-dialog/mat-toolbar/button');

      // create datasets 1
      // click datasets
      await clickButtonByPath(driver2, '//*[@id="cdk-accordion-child-1"]/mat-action-row[2]/button');
      // click define new datasets
      await clickButtonByPath(driver2, '/html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-dataset/mat-toolbar/button');
      // click select database and choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database")]', '//mat-option/span[contains(.,"mydb21")]');
      // click select address and choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Address")]', '//mat-option/span[contains(.,"209.97.173.183")]');
      // click select port and choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Port")]', '//mat-option/span[contains(.,"5432")]');
      // click select database user and choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database User")]', '//mat-option/span[contains(.,"eringga")]');
      // click input table name and set value
      await setFormInputByPath(driver2, '//input[@placeholder="Table Name"]', 'dataset_full')
      // click input description and set value
      await setFormInputByPath(driver2, '//input[@placeholder="Description"]', 'first dataset diff test')
      // click select restriction preference nad choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Restriction Preference")]', '//mat-option/span[contains(.,"Full Disclosure")]');
      // click input price and set value
      await setFormInputByPath(driver2, '//input[@placeholder="Price"]', '50')
      // click add button
      await clickButtonByPath(driver2, '//*[@id="mat-dialog-2"]/configure-dataset-dialog/form/div[3]/button')
      // click yes button
      await clickButtonByPath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
      // wait element to be displayed 
      await getElementByXpath(driver2, '//*[@id="mat-dialog-2"]/configure-dataset-dialog/div/span[contains(.,"Dataset Added")]')
      // click close button
      await clickButtonByPath(driver2, '//*[@id="mat-dialog-2"]/configure-dataset-dialog/mat-toolbar/button');

      // create datasets 2
      // click define new datasets
      await clickButtonByPath(driver2, '/html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-dataset/mat-toolbar/button');
      // click select database and choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database")]', '//mat-option/span[contains(.,"mydb22")]');
      // click select address and choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Address")]', '//mat-option/span[contains(.,"209.97.173.183")]');
      // click select port and choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Port")]', '//mat-option/span[contains(.,"5432")]');
      // click select database user and choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database User")]', '//mat-option/span[contains(.,"eringga")]');
      // click input table name and set value
      await setFormInputByPath(driver2, '//input[@placeholder="Table Name"]', 'dataset_full')
      // click input description and set value
      await setFormInputByPath(driver2, '//input[@placeholder="Description"]', 'second dataset diff test')
      // click select restriction preference nad choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Restriction Preference")]', '//mat-option/span[contains(.,"Full Disclosure")]');
      // click input price and set value
      await setFormInputByPath(driver2, '//input[@placeholder="Price"]', '50')
      // click add button
      await clickButtonByPath(driver2, '//*[@id="mat-dialog-3"]/configure-dataset-dialog/form/div[3]/button')
      // click yes button
      await clickButtonByPath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
      // wait element to be displayed 
      await getElementByXpath(driver2, '//*[@id="mat-dialog-3"]/configure-dataset-dialog/div/span[contains(.,"Dataset Added")]')
      // expect to equal dataset added
      await expectToEqualPath(driver2, '//*[@id="mat-dialog-3"]/configure-dataset-dialog/div/span[contains(.,"Dataset Added")]', 'Dataset Added')
    }, 300000
  );

  test("apvm-create new contract 2 datasets different schema", async () => {
    // Login
    await Login(driver, process.env.USERNAME_APVM, process.env.PASSWORD_APVM);
    //get element text analytics contracts
    await getText(driver, '//span[@class="toolbar-title"]', 'Analytics Contracts');
    // Get contract button and click
    await clickButtonByPath(driver, '//button[@class="menu-button mat-raised-button"]');
    // set element form input name
    await setFormInput(driver, 'mat-input-4', "test new contract 2 datasets different schema");
    //set element form input description
    await setFormInput(driver, 'mat-input-5', "description test new contract 2 dataset with differnet schema");
    // set element form input date
    await setDate(driver);
    // click button next step1
    await clickButtonByPath(driver, '//button[@class="menu-button float-right mat-raised-button"]');
    // set element form select option dataset1
    await selectOptionByIdPath(driver, 'mat-select-3', '//mat-option/span/div/div[contains(.,"first dataset diff test")]');
    // set element form select option dataset2 different schema value
    await selectOptionByIdPath(driver, 'mat-select-3', '//mat-option/span/div/div[contains(.,"second dataset diff test")]');
    //click button next step2
    await clickButtonByPath(driver, '//div[@id="cdk-step-content-0-1"]/div/button[@class="menu-button float-right mat-raised-button"]');
    //select value env conda
    await selectOptionById(driver, 'mat-select-4', 'mat-option-11');
    //click step 3
    await clickButtonByPath(driver, '//div[@id="cdk-step-content-0-2"]/div/button[@class="menu-button float-right mat-raised-button"]');
    //click step 4
    await clickButtonByPath(driver, '//div[@id="cdk-step-content-0-3"]/div/button[@class="menu-button float-right mat-raised-button"]');
    // Get success alert and expect success alert text to equal SUBMITTED
    await getText(driver, '//mat-dialog-container[@id="mat-dialog-0"]/configure-contract-dialog/button[@class="menu-button float-right mat-raised-button"]/span[@class="mat-button-wrapper"]/span[@class="menu-button-caption"]', 'SUBMITTED');
  }, 320000);
});
