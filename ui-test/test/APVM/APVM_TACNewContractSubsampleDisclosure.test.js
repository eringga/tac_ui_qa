import {
  getElementByXpath, getCapabilities, Login, SauceLabsConfig, ConfigWebdriverDual, setFormInput, clickButtonByPath, clickButtonById, setDate, selectOptionByIdPath,
  selectOptionById, getText, formInputByPath, selectOptionByPath, assertToEqual
} from '../../utils';

const capabilities = getCapabilities('APVM New Contract Subsample Disclosure', 'APVM New_Contract');

describe('webdriver', () => {

  let driver = SauceLabsConfig(capabilities);
  let driver2 = SauceLabsConfig(capabilities);
  
  ConfigWebdriverDual(driver, driver2, process.env.SITE_URL_APVM_NO_MARKET, process.env.SITE_URL_DOVM);

  test(
    'dovm-create new database and datasets',
    async () => {
      await Login(driver2, process.env.USERNAME_DOVM, process.env.PASSWORD_DOVM);
      //get data request element 
      const output = await getElementByXpath(driver2, '//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-data-request/mat-toolbar/span');
      const outputVal = await output.getText();
      expect(outputVal).toEqual("Data Requests");
      //click menu data element 
      await clickButtonById(driver2, 'mat-expansion-panel-header-1');
      //click menu database element 
      clickButtonByPath(driver2, '//div[@id="cdk-accordion-child-1"]/mat-action-row[1]/button');
      //click button configure new database element 
      await clickButtonByPath(driver2, '//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-database/mat-toolbar/button');
      //set form input database name element
      await formInputByPath(driver2, '//input[@placeholder="Database Name"]', 'mydb24');
      //element set form input database description
      await formInputByPath(driver2, '//input[@placeholder="Database Description"]', 'database test 1')
      //element set form input database type
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database Type")]', '//mat-option/span[contains(.,"Postgres")]')
      //element set form input database version
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database Version")]', '//mat-option/span[contains(.,"9.5")]')
      //element set form input address
      await formInputByPath(driver2, '//input[@placeholder="Address"]', '209.97.173.183');
      //element set form input port
      await formInputByPath(driver2, '//input[@placeholder="Port"]', '5432');
      //element set form input database user
      await formInputByPath(driver2, '//input[@placeholder="Database User"]', 'eringga');
      //element set pawword form input
      await formInputByPath(driver2, '//input[@placeholder="Password"]', 'makeithappen');
      //click element button add
      clickButtonByPath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
      //click element button yes
      await clickButtonByPath(driver2, '//*[@id="mat-dialog-0"]/configure-database-dialog/div/button[2]');
      //wait until database created
      await getElementByXpath(driver2, '//html/body/div[2]/div[2]/div/mat-dialog-container/configure-database-dialog/div/button/span/span[contains(.,"ADDED")]')
      //click close button
      await clickButtonByPath(driver2, '//*[@id="mat-dialog-0"]/configure-database-dialog/mat-toolbar/button');

      //create database 2
      //click element button configure new database
      await clickButtonByPath(driver2, '//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-database/mat-toolbar/button');
      //element set form input database name
      await formInputByPath(driver2, '//input[@placeholder="Database Name"]', 'mydb25');
      //element set form input database description
      await formInputByPath(driver2, '//input[@placeholder="Database Description"]', 'database test 2')
      //element set form input database type
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database Type")]', '//mat-option/span[contains(.,"Postgres")]')
      //element set form input database version
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database Version")]', '//mat-option/span[contains(.,"9.5")]')
      //element set form input address
      await formInputByPath(driver2, '//input[@placeholder="Address"]', '209.97.173.183');
      //element set form input port
      await formInputByPath(driver2, '//input[@placeholder="Port"]', '5432');
      //element set form input database user
      await formInputByPath(driver2, '//input[@placeholder="Database User"]', 'eringga');
      //element set pawword form input
      await formInputByPath(driver2, '//input[@placeholder="Password"]', 'makeithappen');
      //click element button add
      await clickButtonByPath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
      //click element button yes
      await clickButtonByPath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
      //wait until database created
      await getElementByXpath(driver2, '//html/body/div[2]/div[2]/div/mat-dialog-container/configure-database-dialog/div/button/span/span[contains(.,"ADDED")]')
      //click close button
      await clickButtonByPath(driver2, '//*[@id="mat-dialog-1"]/configure-database-dialog/mat-toolbar/button');

      // create datasets 1
      // click datasets
      await clickButtonByPath(driver2, '//*[@id="cdk-accordion-child-1"]/mat-action-row[2]/button');
      // click define new datasets
      await clickButtonByPath(driver2, '/html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-dataset/mat-toolbar/button');
      // click select database and choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database")]', '//mat-option/span[contains(.,"mydb24")]');
      // click select address and choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Address")]', '//mat-option/span[contains(.,"209.97.173.183")]');
      // click select port and choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Port")]', '//mat-option/span[contains(.,"5432")]');
      // click select database user and choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database User")]', '//mat-option/span[contains(.,"eringga")]');
      // click input table name and set value
      await formInputByPath(driver2, '//input[@placeholder="Table Name"]', 'dataset_subsample')
      // click input description and set value
      await formInputByPath(driver2, '//input[@placeholder="Description"]', 'first dataset subsample test')
      // click select restriction preference nad choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Restriction Preference")]', '//mat-option/span[contains(.,"Sub-Sample Disclosure")]');
      // click input price and set value
      await formInputByPath(driver2, '//input[@placeholder="Price"]', '50')
      // click add button
      await clickButtonByPath(driver2, '//*[@id="mat-dialog-2"]/configure-dataset-dialog/form/div[3]/button')
      // click yes button
      await clickButtonByPath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
      // wait element to be displayed 
      await getElementByXpath(driver2, '//*[@id="mat-dialog-2"]/configure-dataset-dialog/div/span[contains(.,"Dataset Added")]')
      // click close button
      await clickButtonByPath(driver2, '//*[@id="mat-dialog-2"]/configure-dataset-dialog/mat-toolbar/button');

      // create datasets 2
      // click define new datasets
      await clickButtonByPath(driver2, '/html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-dataset/mat-toolbar/button');
      // click select database and choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database")]', '//mat-option/span[contains(.,"mydb25")]');
      // click select address and choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Address")]', '//mat-option/span[contains(.,"209.97.173.183")]');
      // click select port and choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Port")]', '//mat-option/span[contains(.,"5432")]');
      // click select database user and choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database User")]', '//mat-option/span[contains(.,"eringga")]');
      // click input table name and set value
      await formInputByPath(driver2, '//input[@placeholder="Table Name"]', 'dataset_subsample')
      // click input description and set value
      await formInputByPath(driver2, '//input[@placeholder="Description"]', 'second dataset subsample test')
      // click select restriction preference nad choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Restriction Preference")]', '//mat-option/span[contains(.,"Sub-Sample Disclosure")]');
      // click input price and set value
      await formInputByPath(driver2, '//input[@placeholder="Price"]', '50')
      // click add button
      await clickButtonByPath(driver2, '//*[@id="mat-dialog-3"]/configure-dataset-dialog/form/div[3]/button')
      // click yes button
      await clickButtonByPath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
      // wait element to be displayed 
      await getElementByXpath(driver2, '//*[@id="mat-dialog-3"]/configure-dataset-dialog/div/span[contains(.,"Dataset Added")]')
      // expect to equal dataset added
      await assertToEqual(driver2, '//*[@id="mat-dialog-3"]/configure-dataset-dialog/div/span[contains(.,"Dataset Added")]', 'Dataset Added')
    }, 300000
  );

  test(
    'apvm-create new contract 2 datasets sub sample disclosure',
    async () => {
      // Login
      await Login(driver, process.env.USERNAME_APVM, process.env.PASSWORD_APVM);
      // Get element text analytics contracts
      await getText(driver, '//span[@class="toolbar-title"]', 'Analytics Contracts');
      //click button new contract
      await clickButtonByPath(driver, '//button[@class="menu-button mat-raised-button"]');
      //set element form input name
      await setFormInput(driver, 'mat-input-4', 'test new contract sub sample disclosure');
      //set element form input description
      await setFormInput(driver, 'mat-input-5', 'description test new contract sub sample disclosure');
      //set element form input date
      await setDate(driver);
      //click button next step 1
      await clickButtonByPath(driver, '//button[@class="menu-button float-right mat-raised-button"]');
      // set element form select option dataset1
      await selectOptionByIdPath(driver, 'mat-select-3', '//mat-option/span[contains(.,"first dataset subsample test")]');
      // await selectOptionByIdPath(driver, 'mat-select-3', '//div[@class="mat-select-content ng-trigger ng-trigger-fadeInContent"]/mat-option[24]');
      // set element form select option dataset2
      await selectOptionByIdPath(driver, 'mat-select-3', '//mat-option/span[contains(.,"second dataset subsample test")]');
      // await selectOptionByIdPath(driver, 'mat-select-3', '//div[@class="mat-select-content ng-trigger ng-trigger-fadeInContent"]/mat-option[24]');
      //click next step 2
      await clickButtonByPath(driver, '//div[@id="cdk-step-content-0-1"]/div/button[@class="menu-button float-right mat-raised-button"]');
      // set conda env
      await selectOptionById(driver, 'mat-select-4', 'mat-option-11');
      // set element form select option restriction level 1
      await selectOptionByIdPath(driver, 'mat-select-6', '//div[@class="mat-select-content ng-trigger ng-trigger-fadeInContent"]/mat-option[2]');
      // click button choose verified scripts
      await clickButtonByPath(driver, '//mat-cell[@class="mat-cell cdk-column-verifiedScript mat-column-verifiedScript ng-star-inserted"]/button[@class="menu-button mat-raised-button ng-star-inserted"]')
      //click radio aggregation1
      await clickButtonById(driver, 'mat-radio-8');
      //click button confirm1
      await clickButtonByPath(driver, '//mat-dialog-container[@id="mat-dialog-0"]/configure-verified-script-dialog[@class="ng-star-inserted"]/div/button[@class="menu-button float-right mat-raised-button"]');
      // set element form select option restriction level 2
      await selectOptionByIdPath(driver, 'mat-select-7', '//div[@class="mat-select-content ng-trigger ng-trigger-fadeInContent"]/mat-option[2]');
      // click button choose verified scripts
      await clickButtonByPath(driver, '//mat-cell[@class="mat-cell cdk-column-verifiedScript mat-column-verifiedScript ng-star-inserted"]/button[@class="menu-button mat-raised-button ng-star-inserted"]')
      //click radio aggregation2
      await clickButtonById(driver, 'mat-radio-16');
      //click button confirm2
      await clickButtonByPath(driver, '//mat-dialog-container[@id="mat-dialog-1"]/configure-verified-script-dialog[@class="ng-star-inserted"]/div/button[@class="menu-button float-right mat-raised-button"]');
      //click next step 3
      await clickButtonByPath(driver, '//div[@id="cdk-step-content-0-2"]/div/button[@class="menu-button float-right mat-raised-button"]');
      //click step 4
      await clickButtonByPath(driver, '//div[@id="cdk-step-content-0-3"]/div/button[@class="menu-button float-right mat-raised-button"]');
    }, 320000
  );

});