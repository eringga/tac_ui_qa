import webdriver from "selenium-webdriver";

import {
  getElementByXpath,
  getCapabilities,
  Login,
  ConfigWebdriver,
  SauceLabsConfig,
  clickButtonByPath
} from "../../utils";

const capabilities = getCapabilities("DOVM Authorize Data Request", "DOVM");

describe("webdriver", () => {
  let driver = SauceLabsConfig(capabilities);
  ConfigWebdriver(driver, process.env.SITE_URL_DOVM);

  test("dovm-dataRequest authorize", async () => {
    await Login(driver, process.env.USERNAME_DOVM, process.env.PASSWORD_DOVM);
    //get element data request
    const output = await getElementByXpath(
      driver,
      "//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-data-request/mat-toolbar/span"
    );
    const outputVal = await output.getText();
    expect(outputVal).toEqual("Data Requests");

    // click actions button
    await clickButtonByPath(driver, '//mat-tab-body[@id="mat-tab-content-0-0"]/div/div[2]/mat-table/mat-row[1]/mat-cell[5]/button')
    // click next step1
    await clickButtonByPath(driver, '//div[@id="cdk-step-content-0-0"]/div[2]/button')
    // click next step2
    await clickButtonByPath(driver, '//div[@id="cdk-step-content-0-1"]/div/button[2]')
    // click authorize button
    await clickButtonByPath(driver, '//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-data-request/mat-toolbar/button[2]')
    // click yes button data request authorization popup
    await clickButtonByPath(driver, '//mat-dialog-container[@id="mat-dialog-0"]/auth-data-request-dialog/div/button[2]')
  },160000);
});
