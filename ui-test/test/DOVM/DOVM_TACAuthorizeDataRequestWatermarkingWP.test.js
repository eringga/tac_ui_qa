import {
  getElementByXpath, getCapabilities, Login, SauceLabsConfig,
  ConfigWebdriverDual, setFormInput, clickButtonByPath, clickButtonById, setDate, selectOptionByIdPath,
  selectOptionById, getText, formInputByPath, selectOptionByPath
} from '../../utils';

const capabilities = getCapabilities("DOVM Authorize Data Request with watermarking primary key and no numerical", "DOVM");

describe("webdriver", () => {

  let driver = SauceLabsConfig(capabilities);
  let driver2 = SauceLabsConfig(capabilities);
  // ConfigWebdriver(driver2, process.env.SITE_URL_DOVM);
  ConfigWebdriverDual(driver, driver2, process.env.SITE_URL_APVM_NO_MARKET, process.env.SITE_URL_DOVM)
  test(
    'dovm-create new database and datasets',
    async () => {
      await Login(driver2, process.env.USERNAME_DOVM, process.env.PASSWORD_DOVM);
      //get element data request
      const output = await getElementByXpath(driver2, '//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-data-request/mat-toolbar/span');
      const outputVal = await output.getText();
      expect(outputVal).toEqual("Data Requests");
      //click element menu data
      await clickButtonById(driver2, 'mat-expansion-panel-header-1');
      //click element menu database
      clickButtonByPath(driver2, '//div[@id="cdk-accordion-child-1"]/mat-action-row[1]/button');
      //click element button configure new database
      await clickButtonByPath(driver2, '//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-database/mat-toolbar/button');
      //element set form input database name
      await formInputByPath(driver2, '//input[@placeholder="Database Name"]', 'mydb2');
      //element set form input database description
      await formInputByPath(driver2, '//input[@placeholder="Database Description"]', 'database test')
      //element set form input database type
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database Type")]', '//mat-option/span[contains(.,"Postgres")]')
      //element set form input database version
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database Version")]', '//mat-option/span[contains(.,"9.5")]')
      //element set form input address
      await formInputByPath(driver2, '//input[@placeholder="Address"]', '209.97.173.183');
      //element set form input port
      await formInputByPath(driver2, '//input[@placeholder="Port"]', '5432');
      //element set form input database user
      await formInputByPath(driver2, '//input[@placeholder="Database User"]', 'eringga');
      //element set pawword form input
      await formInputByPath(driver2, '//input[@placeholder="Password"]', 'makeithappen');
      // click element button add
      clickButtonByPath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
      // click element button yes
      await clickButtonByPath(driver2, '//*[@id="mat-dialog-0"]/configure-database-dialog/div/button[2]');
      // wait until database created
      await getElementByXpath(driver2, '//html/body/div[2]/div[2]/div/mat-dialog-container/configure-database-dialog/div/button/span/span[contains(.,"ADDED")]')
      // await getElementByXpath(driver2, '//*[@id="mat-dialog-0"]/configure-database-dialog/div/span')
      // click close button
      await clickButtonByPath(driver2, '//*[@id="mat-dialog-0"]/configure-database-dialog/mat-toolbar/button');

      // click datasets
      await clickButtonByPath(driver2, '//*[@id="cdk-accordion-child-1"]/mat-action-row[2]/button');
      // click define new datasets
      await clickButtonByPath(driver2, '/html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-dataset/mat-toolbar/button');
      // click select database and choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database")]', '//mat-option/span[contains(.,"mydb2")]');
      // click select address and choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Address")]', '//mat-option/span[contains(.,"209.97.173.183")]');
      // click select port and choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Port")]', '//mat-option/span[contains(.,"5432")]');
      // click select database user and choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database User")]', '//mat-option/span[contains(.,"eringga")]');
      // click input table name and set value
      await formInputByPath(driver2, '//input[@placeholder="Table Name"]', 'table1')
      // click input description and set value
      await formInputByPath(driver2, '//input[@placeholder="Description"]', 'table_watermarking 1')
      // click select restriction preference nad choose option value
      await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Restriction Preference")]', '//mat-option/span[contains(.,"Full Disclosure")]');
      // click input price and set value
      await formInputByPath(driver2, '//input[@placeholder="Price"]', '50')
      // click add button
      await clickButtonByPath(driver2, '//*[@id="mat-dialog-1"]/configure-dataset-dialog/form/div[3]/button')
      // click yes button
      await clickButtonByPath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
      // wait element to be displayed 
      await getElementByXpath(driver2, '//*[@id="mat-dialog-1"]/configure-dataset-dialog/div/span[contains(.,"Dataset Added")]')
      // click close button
      await clickButtonByPath(driver2, '//*[@id="mat-dialog-1"]/configure-dataset-dialog/mat-toolbar/button');
    }, 180000);

  test(
    'apvm-create new contract 1 authorized data request watermarking',
    async () => {
      //get login         
      await Login(driver, process.env.USERNAME_APVM, process.env.PASSWORD_APVM);
      //get element text Analytics Contracts
      await getText(driver, '//span[@class="toolbar-title"]', 'Analytics Contracts');
      // Get Button new Contract and click
      await clickButtonByPath(driver, '//button[@class="menu-button mat-raised-button"]');
      // set form input name
      await setFormInput(driver, "mat-input-4", "test create new contract watermarking");
      // set form input description
      await setFormInput(driver, "mat-input-5", "test create new contract watermarking description");
      //set date
      await setDate(driver);
      //click next step 1
      await clickButtonByPath(driver, '//button[@class="menu-button float-right mat-raised-button"]');
      // set element select value dataset 1
      await selectOptionByIdPath(driver, 'mat-select-3', '//mat-option/span/div/div[contains(.,"table_watermarking 1")]');
      //click button next step2
      await clickButtonByPath(driver, '//div[@id="cdk-step-content-0-1"]/div/button[@class="menu-button float-right mat-raised-button"]');
      //select value env conda
      await selectOptionById(driver, 'mat-select-4', 'mat-option-11');
      //click step 3
      await clickButtonByPath(driver, '//div[@id="cdk-step-content-0-2"]/div/button[@class="menu-button float-right mat-raised-button"]');
      //click step 4
      await clickButtonByPath(driver, '//div[@id="cdk-step-content-0-3"]/div/button[@class="menu-button float-right mat-raised-button"]');
      //click submited
      await getText(driver, '//mat-dialog-container[@id="mat-dialog-0"]/configure-contract-dialog/button[@class="menu-button float-right mat-raised-button"]/span[@class="mat-button-wrapper"]/span[@class="menu-button-caption"]', 'SUBMITTED');
    }, 180000
  );

  test("dovm-dataRequest authorize with watermarking primary key and no numerical", async () => {
    // await Login(driver2, process.env.USERNAME_DOVM, process.env.PASSWORD_DOVM);
    // click data request menu
    await clickButtonByPath(driver2, '//*[@id="cdk-accordion-child-1"]/mat-action-row[3]/button/span/span')
    //get element data request
    await getText(driver2, '//span[@class="toolbar-title"]', 'Data Requests');
    //search data
    await setFormInput(driver2, "filterInput", "test create new contract watermarking");
    // click actions button
    await clickButtonByPath(driver2, '//button/span[contains(.,"ACTIONS")]')
    // click next step1
    await clickButtonByPath(driver2, '//button/span/span[contains(.,"NEXT")]')
    // click next step2
    await clickButtonByPath(driver2, '//button[2]/span/span[contains(.,"NEXT")]')
    // click checkbox button wetermark
    await clickButtonByPath(driver2, '//mat-card/mat-checkbox')
    // click edit button
    await clickButtonByPath(driver2, '//button/span/span[contains(.,"EDIT")]')
    // click radio button columns a
    await clickButtonByPath(driver2, '//mat-list/mat-radio-button[1]')
    // click next step1
    await clickButtonByPath(driver2, '//html/body/div/div[2]/div/mat-dialog-container/edit-watermark-dialog/mat-dialog-content/div/form/mat-horizontal-stepper/div[2]/div[1]/div[2]/button')
    //seect data step 2
    await clickButtonByPath(driver2, '//html/body/div/div[2]/div/mat-dialog-container/edit-watermark-dialog/mat-dialog-content/div/form/mat-horizontal-stepper/div[2]/div[2]/div[1]/mat-selection-list/mat-list-option[1]/div/div[2]/div/div[1]')
    //click next step 3
    await clickButtonByPath(driver2, '//html/body/div/div[2]/div/mat-dialog-container/edit-watermark-dialog/mat-dialog-content/div/form/mat-horizontal-stepper/div[2]/div[2]/div[2]/button[2]')
    //Number of digits to preserve check > 15
    await formInputByPath(driver2, "//input[@type='number']", "25");
    //click next step 4
    await clickButtonByPath(driver2, '/html/body/div/div[2]/div/mat-dialog-container/edit-watermark-dialog/mat-dialog-content/div/form/mat-horizontal-stepper/div[2]/div[3]/div[3]/button[2]')
    const numberCheck = await getElementByXpath(driver2, "//div/mat-error[@role='alert']");
    const outputVal = await numberCheck.getText();
    expect(outputVal).toEqual("This field must be at most 15");
    //Number of digits to preserve check < 15
    await formInputByPath(driver2, "//input[@type='number']", "12");
    //click next step 4
    await clickButtonByPath(driver2, '/html/body/div/div[2]/div/mat-dialog-container/edit-watermark-dialog/mat-dialog-content/div/form/mat-horizontal-stepper/div[2]/div[3]/div[3]/button[2]')
    //click next step 5
    await clickButtonByPath(driver2, '//html/body/div/div[2]/div/mat-dialog-container/edit-watermark-dialog/mat-dialog-content/div/form/mat-horizontal-stepper/div[2]/div[4]/div[4]/button[2]')
    //click button confirm
    await clickButtonByPath(driver2, '//button/span/span[contains(.,"CONFIRM")]')
    //click button download ket
    await clickButtonByPath(driver2, '//button[@class="mat-raised-button menu-button-orange float-right ng-star-inserted"]')
    // click button authorize
    await clickButtonByPath(driver2, '//mat-toolbar[@class="mat-toolbar mat-toolbar-single-row ng-star-inserted"]/button[2]');
    //click yes
    await clickButtonByPath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
    //click authorized
    await clickButtonByPath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
  }, 200000);
});
