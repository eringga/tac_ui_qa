import webdriver from 'selenium-webdriver';

import { helper, getElementById, start, stop, getElementByName, getElementByXpath, getCapabilities, Login, Logout, ConfigWebdriver, SauceLabsConfig } from '../../utils';

const capabilities = getCapabilities('DOVM TAC Data Request', 'DOVM');

describe('webdriver', () => {
    let driver = SauceLabsConfig(capabilities);    
    ConfigWebdriver(driver, process.env.SITE_URL_DOVM);

    test(
        'dovm-pending-request',
        async () => {
            //get login
            await Login(driver, process.env.USERNAME_DOVM, process.env.PASSWORD_DOVM);
            //get element text data request
            const output = await getElementByXpath(driver, '//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-data-request/mat-toolbar/span');
            const outputVal = await output.getText();
            expect(outputVal).toEqual("Data Requests");    
        },30000
    );

    test(
        'dovm-authorized-request',
        async () => {
            //click authorized-request
            const clickData = await getElementById(driver, 'mat-tab-label-0-1');
            await clickData.click();     
            //get logout
            await Logout(driver);          
        },30000
    );
});
