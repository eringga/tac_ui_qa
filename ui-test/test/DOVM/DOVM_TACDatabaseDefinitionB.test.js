import webdriver from "selenium-webdriver";

import {
 getElementByXpath, getCapabilities, Login, ConfigWebdriver, SauceLabsConfig, clickButtonByPath, clickButtonById, setFormInput, selectOptionById, expectToBeTruthy
} from '../../utils';

const capabilities = getCapabilities("TAC test databases definition B", "DOVM");

describe("webdriver", () => {
  let driver = SauceLabsConfig(capabilities);
  ConfigWebdriver(driver, process.env.SITE_URL_DOVM);

  test("dovm-database definition B wrong credential", async () => {
    await Login(driver, process.env.USERNAME_DOVM, process.env.PASSWORD_DOVM);
    //get element data request
    const output = await getElementByXpath(
      driver,
      "//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-data-request/mat-toolbar/span"
    );
    const outputVal = await output.getText();
    expect(outputVal).toEqual("Data Requests");
    //click database menu
    await clickButtonById(driver, 'mat-expansion-panel-header-1');
    //click element menu database
    await clickButtonByPath(driver, '//div[@id="cdk-accordion-child-1"]/mat-action-row[1]/button');
    //click configure new database
    await clickButtonByPath(driver, '//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-database/mat-toolbar/button')
    //set form input Database name
    await setFormInput(driver, 'mat-input-5', 'testt');
    //element set form input database description
    await setFormInput(driver, 'mat-input-6', 'test create database');
    //element set form input database type
    await selectOptionById(driver, 'mat-select-4', 'mat-option-17');
    //element set form input database version
    await selectOptionById(driver, 'mat-select-5', 'mat-option-20');
    //element set form input address
    await setFormInput(driver, 'mat-input-7', '209.97.173.183');    
    //element set form input port
    await setFormInput(driver, 'mat-input-8',  '5432'); 
    //element set form input database user
    await setFormInput(driver, 'mat-input-9', 'dovm_user'); 
    //element set pawword form input
    await setFormInput(driver, 'mat-input-10', 'qwerty123');
    //click element button add
    await clickButtonByPath(driver, '//button[@class="float-right mat-raised-button menu-button-orange"]');                  
    //click element button yes
    await clickButtonByPath(driver, '//button[@class="float-right mat-raised-button menu-button-orange"]/span[contains(.,"YES")]'); 
    //expect button added
    const wrongCredential = await getElementByXpath(driver, '//mat-list-item[@class="mat-list-item ng-star-inserted"]/div[contains(.,"Invalid database credentials")]');
    const wrongCredentialVal = await wrongCredential.getText();
    expect(wrongCredentialVal).toEqual("Invalid database credentials");
    //close modal
    await clickButtonByPath(driver, '//mat-toolbar[@class="details-title mat-toolbar mat-toolbar-single-row"]/button[@class="float-right mat-icon-button"]');  

  }, 190000);

  test("dovm-database definition B success", async () => {
    //click configure new database
    await clickButtonByPath(driver, '//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-database/mat-toolbar/button')
    //set form input Database name
    await setFormInput(driver, 'mat-input-11', 'mydb10');
    //element set form input database description
    await setFormInput(driver, 'mat-input-12', 'test create database');
    //element set form input database type
    await selectOptionById(driver, 'mat-select-6', 'mat-option-24');
    //element set form input database version
    await selectOptionById(driver, 'mat-select-7', 'mat-option-27');
    //element set form input address
    await setFormInput(driver, 'mat-input-13', '209.97.173.183');       
    //element set form input port
    await setFormInput(driver, 'mat-input-14',  '5432'); 
    //element set form input database user
    await setFormInput(driver, 'mat-input-15', 'eringga'); 
    //element set pawword form input
    await setFormInput(driver, 'mat-input-16', 'makeithappen');
    //click element button add
    await clickButtonByPath(driver, '//button[@class="float-right mat-raised-button menu-button-orange"]');                  
    //click element button yes
    await clickButtonByPath(driver, '//button[@class="float-right mat-raised-button menu-button-orange"]/span[contains(.,"YES")]'); 
    //expect button added
    const addedSuscces = await getElementByXpath(driver, '//div[@class="prompt ng-star-inserted"]/span[contains(.,"Database added")]');
    expect(addedSuscces).toBeTruthy()

  }, 190000);

});
