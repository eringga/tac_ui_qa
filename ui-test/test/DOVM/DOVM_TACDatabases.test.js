import webdriver from 'selenium-webdriver';

import { helper, getElementById, getElementByName, getElementByXpath, getCapabilities, Login, Logout, ConfigWebdriver, SauceLabsConfig } from '../../utils';

const capabilities = getCapabilities('TAC test databases', 'DOVM');

describe('webdriver', () => {
    let driver = SauceLabsConfig(capabilities);    
    ConfigWebdriver(driver, process.env.SITE_URL_DOVM);

    test(
        'dovm-database',
        async () => {
            await Login(driver, process.env.USERNAME_DOVM, process.env.PASSWORD_DOVM);
            //get element data request
            const output = await getElementByXpath(driver, '//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-data-request/mat-toolbar/span');
            const outputVal = await output.getText();
            expect(outputVal).toEqual("Data Requests");
            //click element menu data
            const clickData = await getElementById(driver, 'mat-expansion-panel-header-1');
            await clickData.click();
            //click element menu database
            const clickDatabase = await getElementByXpath(driver, '//div[@id="cdk-accordion-child-1"]/mat-action-row[1]/button');  
            await clickDatabase.click();      
        },190000
    );

    test(
        'dovm-configure-new-database',
        async () => {
            //click element button configure new database
            const clicknewdatabase = await getElementByXpath(driver, '//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-database/mat-toolbar/button');
            await clicknewdatabase.click();   
            //element set form input database name
            const dbName = await getElementById(driver, 'mat-input-5');
            await dbName.clear();
            await dbName.sendKeys("dbPostgres");
            //element set form input database description
            const dbDesc = await getElementById(driver, 'mat-input-6');
            await dbDesc.clear();
            await dbDesc.sendKeys("database postgress");
            //element set form input database type
            const dbType = await getElementById(driver, 'mat-select-4')
            await dbType.click();
            const dbTypeVal = await getElementById(driver, 'mat-option-17')
            await dbTypeVal.click();
            //element set form input database version
            const dbVersion = await getElementById(driver, 'mat-select-5')
            await dbVersion.click();
            const dbVersionVal = await getElementById(driver, 'mat-option-20')
            await dbVersionVal.click();
            //element set form input address
            const address = await getElementById(driver, 'mat-input-7');
            await address.clear();
            await address.sendKeys("testPostgresdb");
            //element set form input port
            const port = await getElementById(driver, 'mat-input-8');
            await port.clear();
            await port.sendKeys("3306");
            //element set form input database user
            const dbUser = await getElementById(driver, 'mat-input-9');
            await dbUser.clear();
            await dbUser.sendKeys("root");
            //element set pawword form input
            const pwd = await getElementById(driver, 'mat-input-10');
            await pwd.clear();
            await pwd.sendKeys("root");
            //click element button add
            const add = await getElementByXpath(driver, '//button[@class="float-right mat-raised-button menu-button-orange"]');              
            await add.click();
            //click element button yes
            const yes = await getElementByXpath(driver, '//button[@class="float-right mat-raised-button menu-button-orange"]');              
            await yes.click();
        },190000
    );

});
