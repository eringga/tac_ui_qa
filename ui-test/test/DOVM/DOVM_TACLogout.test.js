import webdriver from 'selenium-webdriver';

import { helper, getElementById, start, stop, getElementByName, getElementByXpath, getCapabilities, Login, Logout, ConfigWebdriver, SauceLabsConfig } from '../../utils';

var $ = require('jquery');

const capabilities = getCapabilities('DOVM TAC Logout');

describe('webdriver', () => {
    let driver = SauceLabsConfig(capabilities);    
    ConfigWebdriver(driver, process.env.SITE_URL_DOVM);

    test(
        'dovm-login',
        async () => {
            //get login
            await Login(driver, process.env.USERNAME_DOVM, process.env.PASSWORD_DOVM);
        },30000
    );

    test(
        'dovm-logout',
        async () => {
            //get logout
            await Logout(driver);
        },30000
    );
});
