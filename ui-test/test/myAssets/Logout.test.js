import { 
    getCapabilities,     
    ConfigWebdriver, 
    SauceLabsConfig,
    Login, 
    Logout
} from '../../utils';

const capabilities = getCapabilities('Test Lucerne Logout');

describe('webdriver', () => {
    let driver = SauceLabsConfig(capabilities);
    ConfigWebdriver(driver, process.env.SITE_URL_APVM);

    test(
        'Lucerne test login',
        async () => {
            await Login(driver, process.env.USERNAME_APVM, process.env.PASSWORD_APVM);
        },50000
    );

    test(
        'Lucerne test logout',
        async () => {
            await Logout(driver);
        },50000
    );

});