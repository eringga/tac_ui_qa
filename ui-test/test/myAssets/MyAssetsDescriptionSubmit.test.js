import { 
    getCapabilities, 
    Login, 
    ConfigWebdriver, 
    SauceLabsConfig,
    clickButtonByPath,
    assertToEqual,
    formInputByPath,
    assertButtonEnable,
    assertToBeTruthy,
    assertButtonDisplayed,
    clickButtonByDraftandPublish,
    deleteModelEcrRepo
} from '../../utils';

const capabilities = getCapabilities('myassets description submit');

describe('webdriver', () => {
    let driver = SauceLabsConfig(capabilities);
    ConfigWebdriver(driver, process.env.SITE_URL_APVM);
    
    const random = Math.random().toString(36).substring(7);;
    const MODEL_NAME = 'test_'+ random;

    afterAll(() => {
        return deleteModelEcrRepo(MODEL_NAME);
    });

    test(
        'myassets description submit',
        async () => {
            await Login(driver, process.env.USERNAME_APVM, process.env.PASSWORD_APVM);
            //click menu engine
            await clickButtonByPath(driver, '//span[contains(.,"Engines")]');
            //click menu myassets
            await clickButtonByPath(driver, '//a[contains(.," My Assets")]');
            //click menu myassets
            await clickButtonByPath(driver, '//a[contains(.,"Register New Model")]');
            //check field model url
            await formInputByPath(
                driver, 
                '//input[@formcontrolname="modelUrl"]',
                'hackathon-test-user/mobilenet.tar.gz'
            );
            //check field model name
            await formInputByPath(
                driver, 
                '//input[@formcontrolname="modelName"]',
                MODEL_NAME
            );
            //check field model name
            await formInputByPath(
                driver, 
                '//textarea[@formcontrolname="description"]',
                'test'
            );
            //check field License
            await clickButtonByPath(
                driver, 
                '//pg-select[@ng-reflect-name="license"]'
            );
            //click label License
            await clickButtonByPath(
                driver, 
                '//li[contains(.,"Apache")]'
            );
            //click next
            await clickButtonByPath(
                driver, 
                '//button[contains(.," Next ")]'
            );
            await assertToEqual(
                driver,
                '//h5[contains(.,"Downloading Model")]',
                'Downloading Model'
            );
            await assertToEqual(
                driver,
                '//h5[contains(.," Building Model ")]',
                'Building Model'
            );
            await assertToEqual(
                driver,
                '//h5[contains(.," Testing Model ")]',
                'Testing Model'
            );
            await assertToEqual(
                driver,
                '//h5[contains(.," Storing Model ")]',
                'Storing Model'
            );
            //assert button enable
            await assertButtonDisplayed(
                driver,
                 '//button[contains(.," View Draft and Publish ")]'
            );
            //click view draft
            await clickButtonByDraftandPublish(
                driver, 
                '//button[contains(.," View Draft and Publish ")]'
            );     
            //set input description
            await formInputByPath(
                driver, 
                '//input[@ng-reflect-name="input"]',
                'testinput'
            );  
            //set top_k description
            await formInputByPath(
                driver, 
                '//input[@ng-reflect-name="top_k"]',
                '12345'
            );  

             //set probality description
             await formInputByPath(
                driver, 
                '//input[@ng-reflect-name="probability"]',
                '123456'
            );  
            //set names description
            await formInputByPath(
                driver, 
                '//input[@ng-reflect-name="names"]',
                'testnames'
            ); 
            //click publish model
            await clickButtonByPath(
                driver, 
                '//button[contains(.," Publish Model ")]'
            );
            //assert button publish success
            await assertToBeTruthy(
                driver,
                `//div[@class="col-lg-4 ng-star-inserted"]/pgcard/div/div[1]/div[contains(.,"${MODEL_NAME}")]`,
            );
            //click view
            await clickButtonByPath(
                driver,
                '//button[contains(.," View ")]',
            )
            await clickButtonByPath(
                driver, 
                '//td[contains(.,"testnames")]',
            );     
            //assert input value
            await assertToEqual(
                driver,
                '//td[contains(.,"testinput")]',
                'testinput'
            )
            //assert top_k value
            await assertToEqual(
                driver,
                '//td[contains(.,"12345")]',
                '12345'
            )
            //assert probality value
            await assertToEqual(
                driver,
                '//td[contains(.,"123456")]',
                '123456'
            )
            //assert namse value
            await assertToEqual(
                driver,
                '//td[contains(.,"testnames")]',
                'testnames'
            )
        },160000
    );

});