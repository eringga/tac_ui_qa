import { 
    getCapabilities, 
    Login, 
    ConfigWebdriver, 
    SauceLabsConfig,
    clickButtonByPath,
    formInputByID,
    assertToEqual,
    deleteModelEcrRepo,
    assertToMatch
} from '../../utils';

const capabilities = getCapabilities('myassets test search');

describe('webdriver', () => {
    let driver = SauceLabsConfig(capabilities);
    ConfigWebdriver(driver, process.env.SITE_URL_APVM);

    const MODEL_NAME = 'test_';

    afterAll(() => {
        return deleteModelEcrRepo(MODEL_NAME);
    });

    test(
        'myassets test search',
        async () => {
            await Login(driver, process.env.USERNAME_APVM, process.env.PASSWORD_APVM);
            //click menu engine
            await clickButtonByPath(
                driver, 
                '//span[contains(.,"Engines")]'
            );
            //click menu myassets
            await clickButtonByPath(
                driver, 
                '//a[contains(.," My Assets")]'
            );
            //set field search
            await formInputByID(
                driver, 
                'search',
                MODEL_NAME
            );
            //assert check data search
            await assertToMatch(
                driver,
                '//div[@class="col-lg-4 ng-star-inserted"]/div/pgcard/div/div[1]/div[contains(.,"'+MODEL_NAME+'")]',
                MODEL_NAME
            );
        },130000
    );

});
