import { 
    getCapabilities, 
    Login, 
    ConfigWebdriver, 
    SauceLabsConfig,
    clickButtonByPath,
    formInputByID,
    assertToEqual,
    assertToBeTruthy
} from '../../utils';

const capabilities = getCapabilities('catalog test search');

describe('webdriver', () => {
    let driver = SauceLabsConfig(capabilities);
    ConfigWebdriver(driver, process.env.SITE_URL_APVM);

    const MODEL_NAME = 'test_';

    afterAll(() => {
        return deleteModelEcrRepo(MODEL_NAME);
    });

    test(
        'catalog test search',
        async () => {
            await Login(driver, process.env.USERNAME_APVM, process.env.PASSWORD_APVM);
            //click menu engine
            await clickButtonByPath(
                driver, 
                '//span[contains(.,"Engines")]'
            );
            //set field search
            await formInputByID(
                driver, 
                'search',
                MODEL_NAME
            );
            //assert check data search
            await assertToBeTruthy(
                driver,
                '//div[@class="col-lg-4 ng-star-inserted"]/pgcard/div/div[1]/div[contains(.,"'+MODEL_NAME+'")]',
            );
        },130000
    );

});