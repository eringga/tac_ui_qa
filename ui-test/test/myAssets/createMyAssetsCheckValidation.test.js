import { 
    getCapabilities, 
    Login, 
    ConfigWebdriver, 
    SauceLabsConfig,
    clickButtonByPath,
    assertToEqual,
    formInputByPath
} from '../../utils';

const capabilities = getCapabilities('create myassets check validation');

describe('webdriver', () => {
    let driver = SauceLabsConfig(capabilities);
    ConfigWebdriver(driver, process.env.SITE_URL_APVM);

    test(
        'create myassets check validation',
        async () => {
            await Login(driver, process.env.USERNAME_APVM, process.env.PASSWORD_APVM);
            //click menu engine
            await clickButtonByPath(driver, '//span[contains(.,"Engines")]');
            //click menu myassets
            await clickButtonByPath(driver, '//a[contains(.," My Assets")]');
            //click menu myassets
            await clickButtonByPath(driver, '//a[contains(.,"Register New Model")]');
            //check field model url
            await formInputByPath(
                driver, 
                '//input[@formcontrolname="modelUrl"]',
                ''
            );
            await clickButtonByPath(
                driver, 
                '//label[contains(.,"Model Url")]'
            );
            await assertToEqual(
                driver,
                '//div[@class="validation-errors"]/div/div[contains(.," Model URL is required ")]',
                'Model URL is required'
            );

            //check field model name
            await formInputByPath(
                driver, 
                '//input[@formcontrolname="modelName"]',
                ''
            );
            await clickButtonByPath(
                driver, 
                '//label[contains(.,"Model Name")]'
            );
            await assertToEqual(
                driver,
                '//div[@class="validation-errors"]/div/div[contains(.," Model name is required ")]',
                'Model name is required'
            );

            //check field model name
            await formInputByPath(
                driver, 
                '//textarea[@formcontrolname="description"]',
                ''
            );
            await clickButtonByPath(
                driver, 
                '//label[contains(.,"Description")]'
            );
            await assertToEqual(
                driver,
                '//div[@class="validation-errors"]/div/div[contains(.," Description is required ")]',
                'Description is required'
            );

            //check field License
            await clickButtonByPath(
                driver, 
                '//pg-select[@ng-reflect-name="license"]'
            );
             //click label License
             await clickButtonByPath(
                driver, 
                '//html'
            );
            //check validation
            await assertToEqual(
                driver, 
                '//div[@class="validation-errors"]/div/div[contains(.," License is required ")]',
                'License is required'
            );
        },190000
    );

});