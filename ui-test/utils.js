import webdriver from "selenium-webdriver";
import { ECR } from "aws-sdk";

const until = webdriver.until;
const By = webdriver.By;
const SauceLabsConfig = capabilities => {
  const driver = new webdriver.Builder()
    .usingServer(
      "https://" +
      process.env.SAUCE_USERNAME +
      ":" +
      process.env.SAUCE_ACCESS_KEY +
      "@ondemand.saucelabs.com/wd/hub"
    )
    .withCapabilities(capabilities)
    .build();
  return driver;
};
const SauceLabsConfigDual = capabilities => {
  const driver = new webdriver.Builder()
    .usingServer(
      "https://" +
      process.env.SAUCE_USERNAME +
      ":" +
      process.env.SAUCE_ACCESS_KEY +
      "@ondemand.saucelabs.com/wd/hub"
    )
    .withCapabilities(capabilities)
    .build();
  const driver2 = new webdriver.Builder()
    .usingServer(
      "https://" +
      process.env.SAUCE_USERNAME +
      ":" +
      process.env.SAUCE_ACCESS_KEY +
      "@ondemand.saucelabs.com/wd/hub"
    )
    .withCapabilities(capabilities)
    .build();
  return driver, driver2;
};
const ConfigWebdriverDual = (driver, driver2, site_url, site_url_2) => {
  beforeAll(async () => {
    try {
      driver = driver;
      driver2 = driver2;
      await driver.get(site_url);
      await driver2.get(site_url_2);
    } catch (error) {
      console.error("connection error", error);
    }
    // IMPORTANT! Selenium and Sauce Labs needs more time than regular Jest
  }, 350000);
  afterAll(async () => {
    try {
      await driver.quit();
      await driver2.quit();
    } catch (error) {
      console.error("disconnection error", error);
    }
    // IMPORTANT! Selenium and Sauce Labs needs a lot of time!
  }, 350000);
  afterEach(async function() {
    let reporter = {
      specDone: async result => {
        if (result.status === "failed") {
          await driver.executeScript("sauce:job-result=" + result.status);
          await driver2.executeScript("sauce:job-result=" + result.status);
        } else {
          await driver.executeScript("sauce:job-result=" + result.status);
          await driver2.executeScript("sauce:job-result=" + result.status);
        }
      }
    };
    jasmine.getEnv().addReporter(reporter);
  }, 350000);
};
const ConfigWebdriver = (driver, site_url) => {
  beforeAll(async () => {
    try {
      driver = driver;
      await driver.get(site_url);
    } catch (error) {
      console.error("connection error", error);
    }
    // IMPORTANT! Selenium and Sauce Labs needs more time than regular Jest
  }, 350000);
  afterAll(async () => {
    try {
      await driver.quit();
    } catch (error) {
      console.error("disconnection error", error);
    }
    // IMPORTANT! Selenium and Sauce Labs needs a lot of time!
  }, 350000);
  afterEach(async function() {
    let reporter = {
      specDone: async result => {
        if (result.status === "failed") {
          await driver.executeScript("sauce:job-result=" + result.status);
        } else {
          await driver.executeScript("sauce:job-result=" + result.status);
        }
      }
    };
    jasmine.getEnv().addReporter(reporter);
  }, 350000);
};
const getElementById = async (driver, id) => {
  const el = await driver.wait(until.elementLocated(By.id(id)));
  return await driver.wait(until.elementIsVisible(el));
};
const getElementByName = async (driver, name) => {
  const el = await driver.wait(until.elementLocated(By.name(name)));
  return await driver.wait(until.elementIsVisible(el));
};
const getElementByXpath = async (driver, xpath) => {
  const el = await driver.wait(until.elementLocated(By.xpath(xpath)), 35000);
  return await driver.wait(until.elementIsVisible(el), 35000);
};
const getCapabilities = function($name, $tags = '') {
  return {
    build: process.env.CAPABILITIES_BUILD,
    project: process.env.CAPABILITIES_PROJECT,
    name: $name,
    tags: $tags,
    browserName: process.env.CAPABILITIES_BROWSER,
    version: process.env.CAPABILITIES_VERSION,
    os: process.env.CAPABILITIES_OS,
    extendedDebugging: true,
    screenResolution: '1920x1080',
    idleTimeout: '350',
    acceptInsecureCerts: (process.env.CAPABILITIES_ACCEPT_INSECURE_CERTS == 'true')
  };
};
const Login = async (driver, username, password, username_id, password_id) => {
  if (username_id === undefined) {
    //set form input username
    const usrname = await getElementById(driver, "mat-input-0");
    await usrname.clear();
    await usrname.sendKeys(username);
    //set form input password
    const pass = await getElementById(driver, "mat-input-1");
    await pass.clear();
    await pass.sendKeys(password);
    //click button login
    const lnk = await getElementByXpath(
      driver,
      '//span[contains(.,"LOGIN")]'
    );
    await lnk.click();
  } else {
    const id_user = username_id;
    const pwd_id = password_id;
    //set form input username
    const usrname = await getElementById(driver, id_user);
    await usrname.clear();
    await usrname.sendKeys(username);
    //set form input password
    const pass = await getElementById(driver, pwd_id);
    await pass.clear();
    await pass.sendKeys(password);
    //click button login
    const lnk = await getElementByXpath(
      driver,
      '//span[contains(.,"LOGIN")]'
    );
    await lnk.click();
  }
};
const Logout = async (driver, user_id, logout_id) => {
  if (user_id === undefined) {
    //click menu user
    const clickuser = await getElementById(
      driver,
      "mat-expansion-panel-header-0"
    );
    await clickuser.click();
    //click menu logout
    const logout = await getElementByXpath(
      driver,
      '//div[@id="cdk-accordion-child-0"]/mat-action-row/button'
    );
    await logout.click();
  } else {
    //click menu user
    const clickuser = await getElementById(driver, user_id);
    await clickuser.click();
    //click menu logout
    const logout = await getElementByXpath(driver, logout_id);
    await logout.click();
  }
  //get element title
  const logintitile = await getElementByXpath(
    driver,
    '//span[@class="login-title"]'
  );
  const logintitileVal = await logintitile.getText();
  expect(logintitileVal).toEqual("Trusted Analytics Chain");
};
//refactor
const setFormInput = async (driver, input_id, value) => {
  const formInput = await getElementById(driver, input_id);
  await formInput.clear();
  await formInput.sendKeys(value);
};

const setFormInputByPath = async (driver, path, value) => {
  const formInput = await getElementByXpath(driver, path);
  await formInput.clear();
  await formInput.sendKeys(value);
};

const clickButtonByPath = async (driver, path) => {
  const button = await getElementByXpath(driver, path);
  await button.click();
};
const clickButtonById = async (driver, id) => {
  const button = await getElementById(driver, id);
  await button.click();
};
const setDate = async driver => {
  //set element form input date
  const date = await getElementById(driver, "mat-input-6");
  await date.clear();
  const getDate = new Date();
  const getDateVal =
    (getDate.getMonth() > 8
      ? getDate.getMonth() + 1
      : "0" + (getDate.getMonth() + 1)) +
    "/" +
    (getDate.getDate() > 9 ? getDate.getDate() : "0" + getDate.getDate()) +
    "/" +
    getDate.getFullYear();
  await date.sendKeys(getDateVal);
};
const selectOptionByIdPath = async (driver, id, value) => {
  //set element form option
  const formInput = await getElementById(driver, id);
  await formInput.click();
  //set element select value option
  const option = await getElementByXpath(driver, value);
  await option.click();
};

const selectOptionByPath = async (driver, path, value) => {
  //set element form option
  const formInput = await getElementByXpath(driver, path);
  await formInput.click();
  //set element select value option
  const option = await getElementByXpath(driver, value);
  await option.click();
};

const selectOptionById = async (driver, id, value) => {
  //set element form option
  const formInput_id = await getElementById(driver, id);
  await formInput_id.click();
  //set element select value option
  const option_id = await getElementById(driver, value);
  await option_id.click();
};
const getText = async (driver, path, text) => {
  const output = await getElementByXpath(driver, path);
  const outputVal = await output.getText();
  expect(outputVal).toEqual(text);
};
const dovmChooseExpansionMenu = async (driver, value) => {
  const openNav = await getElementById(driver, "mat-expansion-panel-header-1");
  openNav.click();
  const menuExp = await getElementByXpath(driver, value);
  menuExp.click();
};
const clickRadioButtonById = async (driver, radioButtonId) => {
  const radioButtonById = await getElementById(driver, radioButtonId);
  await radioButtonById.click();
};
const expectToBeTruthy = async (driver, id) => {
  const elementId = await getElementById(driver, id);
  expect(elementId).toBeTruthy();
};
const expectToBeTruthyPath = async (driver, path) => {
  const elementPath = await getElementByXpath(driver, path)
  expect(elementPath).toBeTruthy()
}

const expectToEqualPath = async (driver, path, assert) => {
  const elementPath = await getElementByXpath(driver, path)
  const elementText = await elementPath.getText()
  expect(elementText).toEqual(assert)
}
const assertToEqual = async (driver, path, output) => {
    const check = await getElementByXpath(driver, path);
    const assert = await check.getText();
    expect(assert).toEqual(output); 
};
const formInputByPath = async (driver, path, value) => {
    const element = await getElementByXpath(driver, path);
    await element.clear();
    await element.sendKeys(value);
}
const assertButtonEnable = async (driver, path) => {
  const check = await getElementByXpath(driver, path);
  expect(check.isEnabled()).toBeTruthy(); 
};
const assertToBeTruthy = async (driver, path) => {
  const check = await getElementByXpath(driver, path);
  expect(check).toBeTruthy(); 
};
const clickButtonByDraftandPublish = async (driver, path) => {
  const el = await driver.wait(until.elementLocated(By.xpath(path)), 50000);
  const button = await driver.wait(until.elementIsEnabled(el), 50000);
  button.click();
}

const assertButtonDisable = async (driver, path) => {
  const check = await driver.findElement(By.xpath(path)).isEnabled();
  expect(check).toBeFalsy();
};

const formInputByID = async (driver, path, value) => {
  const element = await getElementById(driver, path);
  await element.clear();
  await element.sendKeys(value);
}

const assertToMatch = async (driver, path, output) => {
  const check = await getElementByXpath(driver, path);
  const assert = await check.getText();
  expect(assert).toMatch(output); 
};

const assertButtonDisplayed = async (driver, path) => {
  const check = await getElementByXpath(driver, path); 
  expect(check.isDisplayed()).toBeTruthy(); 
};


/**
 * Deletes the given model name from AWS ECR repository.
 *
 * Expectations:
 * The following environment variables must be set:
 * 1. AWS_ACCESS_KEY_ID
 * 2. AWS_SECRET_ACCESS_KEY
 * 3. AWS_DEFAULT_REGION
 *
 * @param {String} modelName REQUIRED. The model name to delete.
 * @param {String} publicKey OPTIONAL. The public key representing the owner of the model. DEFAULT: undefined
 * TO BE IMPLEMENTED: If a public key is not provided, it will be obtained.
 * @param {String} prefix OPTIONAL. Prefix for the repository name that identifies it as a model. DEFAULT: tam_model_
 * @param {String} separator OPTIONAL. Separator character between the public key and model name. DEFAULT: _
 * @param {Boolean} force OPTIONAL. If a repository contains images, forces the deletion.  DEFAULT: true
 */
const deleteModelEcrRepo = async (
  modelName,
  publicKey,
  prefix = 'tam_model_',
  separator = '_',
  force=true) => {

  const ecr = new ECR({
    apiVersion: '2015-09-21',
    region: process.env['AWS_DEFAULT_REGION']
  });

  if(!publicKey){
    console.log('Obtaining public key');
    // TODO obtain the public key by SSHing
    // Workaround: hardcoded public key that must be updated every time the AMI is updated
    publicKey = 'efv3offnfvxanz6ej6qeb7gjrsemg2gocw9uygfwympv';
  }

  const repositoryName = `${prefix}${publicKey}${separator}${modelName}`;
  const ecrParams = {
    force: force,
    repositoryName: repositoryName
  };
  console.log(`Deleting ECR repo with name: ${repositoryName}`);
  try{
    const ecrResponse = await ecr.deleteRepository(ecrParams).promise();
    console.log(`Successfully deleted ECR repo with name ${repositoryName}`);
    return ecrResponse;
  }catch(error){
    console.warn('Error occurred when attempting to delete ECR repo');
    // Only warn if the repository was not found
    if(error.code == 'RepositoryNotFoundException'){
      console.warn(error.message);
    }else{ // Propagate all other errors, documented at:
      // https://docs.aws.amazon.com/AmazonECR/latest/APIReference/API_DeleteRepository.html
      throw error;
    }
  }
};

export {
  getElementById,
  getElementByName,
  getElementByXpath,
  getCapabilities,
  Login,
  Logout,
  ConfigWebdriver,
  SauceLabsConfig,
  setFormInput,
  formInputByID,
  clickButtonByPath,
  clickButtonById,
  setDate,
  assertToEqual,
  formInputByPath,
  assertToBeTruthy,
  assertButtonEnable,
  assertButtonDisable,
  clickButtonByDraftandPublish,
  selectOptionByIdPath,
  selectOptionById,
  getText,
  dovmChooseExpansionMenu,
  clickRadioButtonById,
  ConfigWebdriverDual,
  expectToBeTruthy,
  expectToBeTruthyPath,
  expectToEqualPath,
  setFormInputByPath,
  selectOptionByPath,
  assertToMatch,
  assertButtonDisplayed,
  deleteModelEcrRepo
};
